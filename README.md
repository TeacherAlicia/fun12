# FUN12

## Pokemon - Database connectie
**Niveau beginner**

![alt text](https://gitlab.com/TeacherAlicia/fun12/raw/master/Pokedex/preview.png)

In deze applicatie zie je hoe je een db-connectie met je C# applicatie kan maken. 
Hierin zit een aparte db-klasse waarin een connectie met de db wordt gemaakt.

Extra opgave: maak de applicatie af!