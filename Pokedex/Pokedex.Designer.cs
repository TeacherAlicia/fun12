﻿namespace FUN12
{
    partial class Pokedex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbSpecies = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbSpecies
            // 
            this.lbSpecies.FormattingEnabled = true;
            this.lbSpecies.ItemHeight = 20;
            this.lbSpecies.Location = new System.Drawing.Point(12, 51);
            this.lbSpecies.Name = "lbSpecies";
            this.lbSpecies.Size = new System.Drawing.Size(439, 364);
            this.lbSpecies.TabIndex = 0;
            // 
            // Pokedex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 428);
            this.Controls.Add(this.lbSpecies);
            this.Name = "Pokedex";
            this.Text = "Pokedex";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbSpecies;
    }
}

