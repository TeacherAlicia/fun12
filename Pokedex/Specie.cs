﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FUN12
{
    class Specie
    {
        private int specieID;
        private string name;
        private Type type;

        private Database database = new Database();

        public Specie(int specieID = -1)
        {
            if (specieID > -1)
            {
                string[] specie = database.GetSpecie(specieID);

                this.specieID = specieID;
                this.name = specie[1];
                this.type = new Type(Convert.ToInt32(specie[2]));
            }
        }

        public override string ToString()
        {
            return this.name + " - " + this.type;
        }
    }
}
