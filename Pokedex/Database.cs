﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace FUN12
{
    class Database
    {
        //connectionString: verwijzing naar de db (telefoonnummer)
        private static string connectionString = @"<verwijzing te vinden bij properties>";
        //SqlConnecton: maakt de connectie met de database (belknop)
        private SqlConnection conn =new SqlConnection(connectionString);

        public Database()
        {
            //Opent de connectie (iemand neemt de telefoon op)
            this.conn.Open();
        }

        public string[] GetSpecie(int specieID)
        {
            string[] specie = new string[3];

            try
            {
                string query = "SELECT * FROM Specie WHERE SpecieID = @SpecieID";
                SqlCommand cmd = new SqlCommand(query, this.conn);
                //het is meest net om parameters te gebruiken ivm sqlinjectie.
                cmd.Parameters.AddWithValue("@SpecieID", specieID);

                //bij using wordt het object (SqlDataReader) meteen weggegooid als het uit de scope gaat.
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();

                    specie[0] = reader["SpecieID"].ToString();
                    specie[1] = reader["Name"].ToString();
                    specie[2] = reader["TypeID"].ToString();
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }

            this.conn.Close();

            return specie;
        }

        public List<int> GetSpecies()
        {
            List<int> species = new List<int>();

            try
            {
                string query = "SELECT SpecieID FROM Specie ORDER BY Name";
                SqlCommand cmd = new SqlCommand(query, this.conn);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int specie = Convert.ToInt32(reader["SpecieID"]);

                        species.Add(specie);
                    }
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }

            this.conn.Close();

            return species;
        }

        public string[] GetType(int typeID)
        {
            string[] type = new string[2];

            try
            {
                string query = "SELECT * FROM Type WHERE TypeID = @TypeID";
                SqlCommand cmd = new SqlCommand(query, this.conn);
                cmd.Parameters.AddWithValue("@TypeID", typeID);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();

                    type[0] = reader["TypeID"].ToString();
                    type[1] = reader["Name"].ToString();
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }

            this.conn.Close();

            return type;
        }
    }
}
